# Car Color Recognition Project

This project focuses on recognizing the colors of cars using various deep learning models trained through transfer learning techniques. Both feature extraction and fine-tuning methods have been employed to achieve high accuracy in car color classification tasks.

## Overview

The project utilizes state-of-the-art deep learning architectures such as EfficientNet (B0, B1), ResNet (V1), and MobileNet (V1) as base models for transfer learning. By leveraging pre-trained weights and fine-tuning on a custom car color dataset, the models are capable of accurately identifying and classifying car colors from input images. With accuracy rates consistently exceeding 88%, the models demonstrate robust performance across a variety of car colors and lighting conditions.

## Key Features

- **Transfer Learning Techniques**: Both feature extraction and fine-tuning methods are employed to leverage pre-trained models for car color recognition tasks.
- **Model Selection**: EfficientNet (B0, B1), ResNet (V1), and MobileNet (V1) architectures are chosen for their balance of accuracy and computational efficiency.
- **High Accuracy**: All trained models achieve accuracy rates exceeding 90%, ensuring reliable performance in car color classification tasks.
- **Versatility**: The trained models can be integrated into various applications, including traffic monitoring systems, smart parking solutions, and automotive industry applications.

## Model Performance

The performance of each trained model is summarized below:

| Model                       | Accuracy   |
|-----------------------------|------------|
| EfficientNet B0             | 89.4%      |
| EfficientNet B1             | 87.8%      |
| ResNet V1                   | 92.4%      |
| MobileNetV3Small            | 87.8%      |
| Fine-tuned EfficientNet B0  | 95.4%      |

## Example Images

![Car Color Recognition Example](test_result7.png)

Above is an example image showcasing the capabilities of the trained models in accurately recognizing different colors of cars.

## Conclusion

The Car Color Recognition Project demonstrates the effectiveness of transfer learning techniques in training deep learning models for car color classification tasks. By leveraging pre-trained architectures and fine-tuning on custom datasets, the models achieve high accuracy rates on the test dataset

